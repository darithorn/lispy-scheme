cd src/modules
csc -c -J ../utility ../errors ../parser ../macro ../hir ../type-info ../static ../hir-optimizer
cd ..
csc main.scm utility.scm errors.scm parser.scm macro.scm hir.scm type-info.scm static.scm hir-optimizer.scm -I modules/ -o ../bin/main
rm *.o
cd ..

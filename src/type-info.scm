(module type-info *
  (import scheme chicken
	  utility hir macro)
  (require-extension defstruct loop format)
  (declare (unit type-info)
	   (uses utility hir macro))

  (defstruct var-decl name type)
  
  ;; generics and variables are separate so the signatures generated from
  ;; usage can stay separated and be easily checked
  ;; children [(list hir-definitions)] -> children is a list of definitions
  ;;    that can be used to traverse down through scope without having to look
  ;;    at (define)'s mulitple times.
  (defstruct hir-definitions parent children variables types macros generics)
  
  (define (new-hir-definitions #!optional (parent #f))
    (let ((new (make-hir-definitions parent: parent
				     children: '()
				     variables: '()
				     types: '()
				     macros: '()
				     generics: '())))
      (when (hir-definitions? parent) (add-child parent new))
      new))

  (define (add-child defs child)
    (hir-definitions-children-set! defs (cons child
					      (hir-definitions-children defs))))
  
  (define (add-variable-definition defs var)
    (hir-definitions-variables-set! defs (cons var
					       (hir-definitions-variables defs))))

  (define (add-type-definition defs type)
    (hir-definitions-types-set! defs (cons type
					   (hir-definitions-types defs))))

  (define (add-generic-definition defs var)
    (hir-definitions-generics-set! defs (cons var
					      (hir-definitions-generics defs))))

  (define (add-macro-definition defs macro)
    (hir-definitions-macros-set! defs (cons macro
					    (hir-definitions-macros defs))))
  
  (define (get-variable defs name)
    (let ((result (findp (lambda (v)
			   (string=? name (var-decl-name v)))
			 (hir-definitions-variables defs))))
      (if result
	  result
	  (if (hir-definitions? (hir-definitions-parent defs))
	      (get-variable (hir-definitions-parent defs) name)
	      #f))))

  (define (get-type defs name)
    (let ((result (findp (lambda (t)
			   (eq? name (hir-type-name t)))
			 (hir-definitions-types defs))))
      (if result
	  result
	  (if (hir-definitions? (hir-definitions-parent defs))
	      (get-type (hir-definitions-parent defs) name)
	      #f))))
  
  ;; This retrieves the type information for certain HIR nodes.
  ;; Function calls return their call signature.
  ;; Typically function calls to built-ins return a none type.
  ;; A lambda call returns its function signature.
  ;; If type information cannot be retrieved then #f is returned.
  (define (get-type-information defs node)
    (case (hir-node-type node)
      ('define-val
	(evaluate-define defs
			 (hir-node-info (first (hir-node-info node)))
			 (get-type-information defs (second (hir-node-info node)))))
      ('define-type-val
	(evaluate-define defs
			 (hir-node-info (first (hir-node-info node)))
			 (third (hir-node-info node))))
      ('lambda
       (get-lambda-signature defs node))
      ('generic-lambda
       (get-lambda-signature defs node))
      ('symbol
       (let ((var (get-variable defs (hir-node-info node))))
	 (if (var-decl? var)
	     (var-decl-type var)
	     #f)))
      ('define-syntax
	(evaluate-define-syntax defs node))
      ('literal
       (hir-node-info node))
      ('pointer
       (get-pointer-information defs node))
      ('deref
       (get-deref-information defs node))
      ('func-call
       (get-call-signature defs node))
      ('operator
       (get-call-signature defs node))
      ('if
       (get-type-information defs (second (hir-node-info node))))
      ('type
       ;; type literals get reevaluated to determine if the referenced type(s)
       ;; are valid
       (get-type-literal-information defs node))
      (else
       (hir-type-none))))

  (define (evaluate-define defs name type-info)
    (let ((var (make-var-decl name: name
			      type: type-info)))
      (if (eq? (hir-type-name type-info) 'generic-func)
	  (add-generic-definition defs var)
	  (add-variable-definition defs var))
      (hir-type-none)))
  
  (define (get-lambda-signature defs node)
    (let* ((lambda-type (case (hir-node-type node)
			  ('lambda 'func)
			  ('generic-lambda 'generic-func)
			  (else #f)))
	   (fn-def (hir-node-info node))
	   (parameters (map (lambda (p)
			     (get-type-information defs p))
			   (function-definition-parameters fn-def))))
      (if lambda-type
	  (begin
	    (let ((local (new-hir-definitions defs)))
	      (loop for param in (function-definition-parameters fn-def) do
		    (add-variable-definition local (make-var-dec
						    name: (function-parameter-name param)
						    type: (get-type-information
							   local
							   (function-parameter-type param)))))
	      (loop for expr in (function-definition-body fn-def) do
		    (get-type-information local expr)))
	    (make-hir-type
	     name: lambda-type
	     data: `(,(get-type-information
		       defs
		       (function-definition-return-type fn-def))
		     ,parameters)))
	  #f)))

  (define (evaluate-define-syntax defs node)
    (add-macro-definition defs (hir-node-info node))
    (hir-type-none))

  (define (get-pointer-information defs node)
    (make-hir-type
     name: 'pointer
     data: (get-type-information defs (hir-node-info node))))

  (define (get-deref-information defs node)
    (make-hir-type
     name: 'deref
     data: (get-type-information defs (hir-node-info node))))

  ;; TODO: retrieve whether it's calling a generic
  ;; and add the call signature to a list of usages
  ;; then the hir-optimizer has that information to use without
  ;; having to rely on multiple passes.
  (define (get-call-signature defs node)
    (if (eq? (hir-node-type node) 'func-call)
	(make-hir-type
	 name: 'func-call
	 data: `(,(map (lambda (p)
			 (get-type-information defs p))
		       (function-call-arguments (hir-node-info node)))))
	#f))

  (define (get-type-literal-information defs node)
    (case (hir-type-name (hir-node-info node))
      ('func
       (make-hir-type
	name: 'func
	data: (cons (get-type-literal-information defs (first (hir-type-data (hir-node-info node))))
		    (map (lambda (p)
			   (get-type-literal-information defs p))
			 (second (hir-type-data (hir-node-info node)))))))
      ('pointer
       (make-hir-type
	name: 'pointer
	data: (get-type-literal-information defs (hir-type-data (hir-node-info node)))))
      ('quote
       (hir-node-info node))
      (else
       (get-type defs (hir-type-name node))))))

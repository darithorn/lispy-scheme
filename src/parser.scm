(module parser *
  (import scheme chicken extras)
  (declare (unit parser) (uses errors))
  (import errors)
  (require-extension defstruct loop format)
  
  ;; type -> 'string 'list 'decimal 'integer 'symbol 'quote 'quasiquote
  ;; col -> the column the expression starts at
  ;; row -> the row the expression starts at
  (defstruct form expr type col row)

  (define (pretty-print-form form)
    (case (form-type form)
      ('string
       (format #f "\"~a\" " (form-expr form)))
      ('list
       (format #f "(~a) " (foldr string-append ""
				(map (lambda (e)
				       (pretty-print-form e))
				     (form-expr form)))))
      ('quote
       (format #f "'~a " (pretty-print-form (form-expr form))))
      (else
       (format #f "~a " (form-expr form)))))
  
  (defstruct lexer file contents length col row i)
  
  (define (read-contents port)
    (let loop ((contents "")
	       (output (read-line port)))
      (if (eof-object? output)
	  contents
	  (begin
	    (set! contents (string-append contents output))
	    (loop contents (read port))))))

  (define (string-find c str)
    (let loop ((i 0)
	       (len (string-length str)))
      (if (eq? c (char str i))
	  c
	  (if (>= (+ 1 i) len)
	      #f
	      (loop (+ 1 i) len)))))

  (define (char str i)
    (if (or (<= i -1) (>= i (string-length str)))
	'()
	(string-ref str i)))

  (define (new-lexer file)
    (let ((file-contents (read-contents file)))
      (make-lexer
       file: file
       contents: file-contents
       length: (string-length file-contents)
       col: -1
       row: 1
       i: -1)))

  (define (next-char self)
    (if (>= (lexer-i self) (- (lexer-length self) 1))
        '()
	(begin
	  (lexer-i-set! self (+ 1 (lexer-i self)))
	  (lexer-col-set! self (+ 1 (lexer-col self)))
	  (let ((c (char (lexer-contents self) (lexer-i self))))
	    (when (eq? c #\newline)
	      (lexer-row-set! self (+ 1 (lexer-row self)))
	      (lexer-col-set! self -1))
	    c))))

  (define (cur-char self)
    (char (lexer-contents self) (lexer-i self)))

  (define (ahead-char self)
    (let ((i (+ 1 (lexer-i self))))
      (if (< (length (lexer-contents self)) i)
	  (char (lexer-contents self) (+ 1 (lexer-i self)))
	  '())))

  (define (back-char self)
    (lexer-i-set! self (- (lexer-i self) 1))
    (cur-char self))

  (define *terminators* " ()[]{}#^%&\\`'.,\"")

  (define (is-alpha-terminator c)
    (or (string-find c *terminators*)
	(char-whitespace? c)))

  (define (is-numeric-terminator c)
    (or (is-alpha-terminator c)
	(not (char-numeric? c))))

  (define (valid-char c)
    (and (not (null? c))
	 (not (char-numeric? c))
	 (or (char-alphabetic? c)
	     (not (string-find c *terminators*)))))

  (define (read-until-terminator self #!optional (is-terminator is-alpha-terminator))
    (let ((c (cur-char self))
	  (start (lexer-i self)))
      (loop
       ;; #\space
       ;; hitting EOF isn't an error because EOF is considered to be a terminator
       (when (null? c) (return #f)) ;; hit EOF
       (when (is-terminator c)
	 (back-char self)
	 (return #f)) ;; hit another character don't want to eat it
       (set! c (next-char self)))
      (substring (lexer-contents self) start (+ 1 (lexer-i self)))))

  (define-syntax with-lexer
    (syntax-rules ()
      ((_ (name file-name) body ...)
       (let ((name (new-lexer (open-input-file file-name))))
	 (set! *errors* '())
	 (begin body ...)))))

  (define (parse-symbol lexer)
    (let ((col (lexer-col lexer))
	  (row (lexer-row lexer))
	  (symbol (read-until-terminator lexer)))
      (if (= 0 (string-length symbol))
	  (begin
	    (report-error *error-critical-symbol-length-zero*
			  "This is a bug in the compiler."
			  col row)
	    (make-form expr: '() type: 'symbol col: col row: row))
	  (make-form expr: symbol type: 'symbol col: col row: row))))

  (define (parse-string lexer)
    (let ((col (lexer-col lexer))
	  (row (lexer-row lexer))
	  (escaped '())
	  (c (next-char lexer)) ;; next char because cur char is "
	  (start (lexer-i lexer)))
      (loop
       (when (null? c)
	 (report-error *error-no-terminating-string*
		       "Expected terminating \" for string" col row)
	 (return #f))
       (when (and (not escaped) (eq? c #\")) (return #f))
       (if (eq? c #\\)
	   (set! escaped #t)
	   (set! escaped '()))
       (set! c (next-char lexer)))
      (make-form type: 'string
		 expr: (substring (lexer-contents lexer) start (lexer-i lexer))
		 col: col
		 row: row)))

  (define (parse-number lexer)
    (let ((col (lexer-col lexer))
	  (row (lexer-row lexer))
	  (number (read-until-terminator lexer is-numeric-terminator))
	  (number-type 'integer))
      (if (eq? (next-char lexer) #\.)
	  (begin
	    (next-char lexer)
	    ;; concatenate to create decimal number
	    (set! number (string-append number
					"."
					(read-until-terminator lexer is-numeric-terminator)))
	    (set! number-type 'decimal))
	  (back-char lexer))
      (if (= 0 (length number))
	  (begin
	    (report-error *error-critical-number-length-zero*
			  "This is a bug in the compiler."
			  col row)
	    (make-form expr: '() type: number-type col: col row: row))
	  (make-form expr: number type: number-type col: col row: row))))

  (define (parse-character lexer)
    ;; (cur-char) is #
    (let ((start-col (lexer-col lexer))
	  (start-row (lexer-row lexer)))
      (if (not (eq? (next-char lexer) #\\))
	  (begin
	    (if (or (eq? (cur-char lexer) #\t)
		    (eq? (cur-char lexer) #\f))
		(make-form type: 'boolean
			   expr: (cur-char lexer)
			   col: start-col row: start-row)
		(report-error *error-no-backslash* "Expected \\ after #" start-col start-row)))
	  (let ((c (next-char lexer)))
	    (if (char-alphabetic? c)
		(begin
		  ;; handle explicitly typed characters like #\space #\newline #\tab 
		  (let* ((col (lexer-col lexer))
			 (row (lexer-row lexer))
			 (explicit (read-until-terminator lexer))
			 (character (cond
				     ((equal? explicit "space") #\space)
				     ((equal? explicit "tab") #\tab)
				     ((equal? explicit "newline") #\newline)
				     ;; if it's a single character
				     ((= (string-length explicit) 1)
				      (char explicit 0))
				     (#t '()))))
		    (if (not (null? character))
			(make-form expr: (make-string 1 character)
				   type: 'character
				   col: start-col
				   row: start-row)
			(report-error *error-unknown-character*
				      (format #f
					      "~a is not a valid character" explicit)
				      col row))))
		;; non-alphabetic characters like #\( or #\0
		(make-form expr: (make-string 1 c) type: 'character
			   col: start-col row: start-row))))))

  ;; This can parse either pointers or dereferences
  ;; TODO: possibly support %^foo
  (define (parse-memory-handlers lexer #!optional (type 'pointer))
    (let ((col (lexer-col lexer))
	  (row (lexer-row lexer))
	  (f (parse-value lexer (next-char lexer)))) 
      (make-form type: type
		 expr: f
		 col: col
		 row: row)))

  ;; This parses an ellipsis ... used for macro variadic patterns
  (define (parse-ellipsis lexer)
    (let ((successful #t)
	  (i 1))
      (loop
       (if (eq? (next-char lexer) #\.)
	   (begin
	     (set! i (+ 1 i))
	     (when (> i 3)
	       (report-error *error-ellipsis-too-many-dots*
			     "An ellipsis is only three dots, got more than three."
			     (lexer-col lexer) (lexer-row lexer))
	       (set! successful #f)
	       (return #f)))
	   (begin
	     (when (< i 3)
	       (report-error *error-improper-ellipsis*
			     (format #f "An ellipsis must have three dots, got ~a"
				     (cur-char lexer))
			     (lexer-col lexer) (lexer-row lexer))
	       (set! successful #f))
	     (back-char lexer)
	     (return #f))))
      (when successful
	(make-form type: 'ellipsis
		   expr: "..."
		   col: (lexer-col lexer)
		   row: (lexer-col lexer)))))
  
  (define (parse-value lexer c) 
    (cond
     ((eq? c #\;)
      (loop
       (when (eq? c #\newline) (return #f))
       (set! c (next-char lexer))))
     ((eq? c #\()
      (parse-list lexer))
     ((eq? c #\.)
      (parse-ellipsis lexer)) 
     ((valid-char c)
      (parse-symbol lexer))
     ((char-numeric? c)
      (parse-number lexer))
     ((eq? c #\#)
      (parse-character lexer))
     ((eq? c #\")
      (parse-string lexer))
     ((eq? c #\^)
      ;; this makes it easier on the type checker to determine pointers
      (parse-memory-handlers lexer))
     ((eq? c #\%)
      (parse-memory-handlers lexer 'deref))
     ((eq? c #\')
      (let ((col (lexer-col lexer))
	    (row (lexer-row lexer))) 
	(make-form expr: (parse-value lexer (next-char lexer))
		   type: 'quote
		   col: col
		   row: row)))
     (#t
      (report-error *error-unexpected-character*
		    (format #f "Unexpected character ~a" c)
		    (lexer-col lexer) (lexer-row lexer)))))

  (define (parse-list lexer #!optional (top #f))
    ;; (cur-char) is (
    (let ((new-list '())
	  (row (lexer-row lexer))
	  (col (lexer-col lexer)))
      (loop
       (let ((c (next-char lexer)))
	 (loop 
	  (unless (or (eq? c #\space)
		      (eq? c #\newline)
		      (eq? c #\tab)) (return #f))
	  (set! c (next-char lexer)))
	 (when (null? c)
	   (unless top
	     ;; don't report an error if it's the top level (implicit list)
	     (report-error *error-no-terminating-paren*
			   "Unexpected end of list. Expected terminating parenthesis."
			   col row))
	   (return #f))
	 ;; if list terminator
	 (when (eq? c #\)) (return #f))
	 ;; parse a value
	 (let ((tmp (parse-value lexer c)))
	   ;; is only null when there's an error while parsing
	   (unless (null? tmp)
	     (set! new-list (cons tmp new-list))))))
      (make-form expr: (reverse new-list) type: 'list col: col row: row))))

(module errors *
  (import scheme chicken extras)
  (declare (unit errors))
  (require-extension defstruct)
  
  (define *error-critical-unknown-built-in* -100)

  (define *error-critical-null-form* -3)
  (define *error-critical-symbol-length-zero* -2)
  (define *error-critical-number-length-zero* -1)
  (define *error-no-terminating-paren* 1)
  (define *error-no-backslash* 2)
  (define *error-unexpected-character* 3)
  (define *error-unknown-character* 4)
  (define *error-no-terminating-string* 5)
  (define *error-improper-memory-handler* 6)
  (define *error-improper-ellipsis* 7)
  (define *error-ellipsis-too-many-dots* 8)


  (define *error-improper-arg-type* 100)
  (define *error-list-element-not-same* 101)
  (define *error-function-call-empty* 102)
  (define *error-function-call-improper-name* 103)
  (define *error-defined-type-as-variable* 104)
  (define *error-deref-type* 105)
  (define *error-deref-undefined-var* 106)
  (define *error-deref-non-pointer* 107)
  (define *error-usage-of-undefined* 108)
  (define *error-incorrect-return-type* 109)
  (define *error-defined-is-not-function* 110)
  (define *error-incorrect-argument-type* 111)
  (define *error-incorrect-arity* 112)
  (define *error-improper-name* 113)
  (define *error-redefinition* 114)
  (define *error-variable-type-val-different* 115)
  (define *error-improper-macro-body* 116)
  (define *error-improper-macro-call* 117)
  (define *error-if-requires-bool* 118)
  (define *error-types-not-same* 119)
  (define *error-improper-body* 120)
  (define *error-missing-return-type* 121)
  (define *error-improper-call-built-in* 122)
  (define *error-not-type* 123)
  (define *error-improper-pointer* 124)
  (define *error-improper-type-literal* 125)
  (define *error-improper-syntax-rules-call* 126)

  (define *error-deformed-macro-pattern* 200)
  (define *error-misplaced-ellipsis* 201)
  (define *error-illegal-ellipsis* 202)
  (define *error-illegal-pattern-after-ellipsis* 203)

  (define *errors* '()) 

  (defstruct compiler-error col row code msg)

  (define (error->string err)
    (format #f "[E~a] <~a:~a> ~a~%"
	    (compiler-error-code err)
	    (compiler-error-row err)
	    (compiler-error-col err)
	    (compiler-error-msg err)))

  ;; TODO: eventually create meaningful error codes
  ;; col and row are there to specify more specific col/row
  ;; for example: the beginning of a list if it wasn't terminated instead of the end of the file
  (define (report-error code msg col row)
    (set! *errors* (cons (make-compiler-error
			  col: col
			  row: row
			  code: code
			  msg: msg)
			 *errors*))
    '()))

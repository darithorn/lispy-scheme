(module utility *
  (import scheme chicken)
  (declare (unit utility))
  (require-extension loop)

  (define (nth list i)
    (if (>= i (length list))
	'()
	(list-ref list i)))

  (define (first l) (nth l 0))
  (define (second l) (nth l 1))
  (define (third l) (nth l 2))
  (define (fourth l) (nth l 3))
  (define (fifth l) (nth l 4))
  (define (rest l) (list-tail l 1))
  
  (define (find value list)
    (findp (lambda (v) (eq? value v)) list))

  (define (findp pred list)
    (loop for v in list do
	  (when (pred v) (return v)))))

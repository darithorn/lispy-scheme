(module static *
  (import scheme chicken)
  (require-extension defstruct loop format)
  (declare (unit static)
	   (uses utility errors hir type-info))
  (import utility errors hir type-info)

  ;; types -> a list of hir-type
  ;; variables -> a list of var-decl
  (defstruct hir-scope parent types variables)

  (define (new-hir-scope parent)
    (make-hir-scope parent: parent types: '() variables: '()))

  (define (add-variable scope var)
    (hir-scope-variables-set! scope (cons var
					  (hir-scope-variables scope))))

  (define (add-type scope type)
    (hir-scope-types-set! scope (cons type
				      (hir-scope-types scope))))

  (define (is-variable-defined scope name)
    (let ((result #f))
      (loop for var in (hir-scope-variables scope) do
	    (when (string=? (var-decl-name var) name)
	      (set! result var)
	      (return result)))
      (if result
	  result
	  (if (null? (hir-scope-parent scope))
	      #f
	      (is-variable-defined (hir-scope-parent scope) name)))))

  (define (is-type-defined scope name)
    (let ((result #f))
      (loop for type in (hir-scope-types scope) do
	    (when (eq? (hir-type-name type) (hir-type-name name))
	      (set! result type)
	      (return result)))
      (if result
	  result
	  (if (null? (hir-scope-parent scope))
	      #f
	      (is-type-defined (hir-scope-parent scope) name)))))

  (define (analyze scope node)
    (when node
      (case (hir-node-type node)
	('define-val
	  (analyze-define-val scope node))
	('define-type-val
	  (analyze-define-type-val scope node))
	('lambda
	    (analyze-lambda scope node))
	('generic-lambda
	 (analyze-generic-lambda scope node))
	('set
	 (analyze-set! scope node))
	('if
	 (analyze-if scope node))
	#|
	;; This is only called during specific times
	;; because only at specific times are type literals specifically created by the HIR
	('type
	(analyze-hir-type scope node)) |#
	('pointer
	 (analyze-pointer scope node))
	('deref
	 (analyze-deref scope node))
	('literal
	 (hir-node-info node))
	('symbol
	 (analyze-symbol scope node))
	('quote
	 ;; TODO
	 )
	('func-call
	 (analyze-func-call scope node))
	(else #f))))

  (define (analyze-define-val scope node)
    (let ((name (first (hir-node-info node)))
	  (value (second (hir-node-info node))))
      (add-variable scope (make-var-decl
			   name: (hir-node-info name)
			   type: (analyze scope value)))
      (hir-type-none)))

  (define (analyze-define-type-val scope node)
    (let ((name (first (hir-node-info node)))
	  (type (analyze-hir-type scope (second (hir-node-info node))))
	  (value (analyze scope (third (hir-node-info node)))))
      (when (and type value)
	(unless (hir-same-type type value)
	  (report-error *error-variable-type-val-different*
			(format #f "Expected ~a got ~a."
				(hir-type-pretty-print type)
				(hir-type-pretty-print value))
			(hir-node-col node) (hir-node-row node)))
	(add-variable scope (make-var-decl
			     name: (hir-node-info name)
			     type: type)))
      (hir-type-none)))

  (define (analyze-lambda scope node)
    (let ((definition (hir-node-info node))
	  (local-scope (new-hir-scope scope)))
      (let ((return-type (analyze-hir-type scope (function-definition-return-type definition)))
	    (parameter-types '()))
	;; don't need to check return-type because it's already checked when we analyze it
	(loop for parameter in (function-definition-parameters definition) do
	      (let ((parameter-type (analyze-hir-type scope (function-parameter-type parameter))))
		;; make sure the parameter type is a defined type
		(if parameter-type
		    (begin
		     ;; add parameter-type to the parameter-types list for later use
		     ;; and add the parameter to the local scope
		     (set! parameter-types (cons parameter-type
						 parameter-types))
		     (add-variable local-scope (make-var-decl
						name: (function-parameter-name parameter)
						type: parameter-type)))
		    (report-error *error-usage-of-undefined*
				  (format #f "Parameter type ~a is not defined as a type"
					  (hir-type-pretty-print (hir-node-info
								  (function-parameter-type parameter))))
				  (hir-node-col (function-parameter-type parameter))
				  (hir-node-row (function-parameter-type parameter))))))
	;; analyze each body expression
	;; since type literals are separated we don't need any special checking
	;; (-> i32) would be treated as a function call to the function ->
	;; if it were in the body
	(loop for body in (function-definition-body definition) do
	      (analyze local-scope body))
	
	(make-hir-type
	 name: 'func
	 data: `(,return-type
		 ,(reverse parameter-types))))))

  ;; analyze-generic-lambda retrieves the function signature from the
  ;; node and adds it to the current scope's generics to be used for
  ;; function calls.
  (define (analyze-generic-lambda scope node)
    (let ((definition (hir-node-info node)))
      '()))

  (define (analyze-set! scope node)
    ;; analyze returns the types of the expressions
    (let ((old (analyze scope (first (hir-node-info node))))
	  (new (analyze scope (second (hir-node-info node)))))
      (when (and old new)
	(if (or (hir-same-type old (hir-type-none))
		(hir-same-type new (hir-type-none)))
	    (report-error *error-improper-arg-type*
			  "(set!) cannot be used with none types"
			  (hir-node-col node)
			  (hir-node-row node))
	    (if (not (hir-same-type old new))
		(report-error *error-types-not-same*
			      (format #f "Expected ~a value, got ~a"
				      (hir-type-pretty-print (hir-node-info old))
				      (hir-type-pretty-print (hir-node-info new)))
			      (hir-node-col old)
			      (hir-node-row old))
		;; (set!) returns a none type
		(hir-type-none))))))

  (define (analyze-if scope node)
    (let ((boolean (analyze scope (first (hir-node-info node))))
	  (true (analyze scope (second (hir-node-info node))))
	  (false (analyze scope (third (hir-node-info node)))))
      (if (not (hir-same-type boolean (hir-type-bool)))
	  (report-error *error-improper-arg-type*
			"The conditional expression must evaluate to a boolean"
			(hir-node-col (first (hir-node-info node)))
			(hir-node-row (first (hir-node-info node))))
	  (if (not (hir-same-type true false))
	      (report-error *error-improper-arg-type*
			    "The true and false expressions must evaluate to the same type"
			    (hir-node-col node)
			    (hir-node-row node))
	      true))))

  (define (analyze-hir-type scope node)
    (when node
      (if (not (eq? (hir-node-type node) 'type))
	  (report-error *error-not-type*
			(format #f "Expected a type got ~a"
				(hir-node-info node))
			(hir-node-col node) (hir-node-row node))
	  (let ((type (hir-node-info node)))
	    (case (hir-type-name type)
	      ('func
	       (let ((result #f))
		 (loop for data in (hir-type-data type) do
		       (let ((defined (is-type-defined scope (hir-node-info data))))
			 (unless defined
			   (report-error *error-usage-of-undefined*
					 (format #f "~a is not defined as a type."
						 (hir-type-pretty-print (hir-node-info data)))
					 (hir-node-col data) (hir-node-row data)))
			 (set! result (cons defined
					    result))))
		 (make-hir-type
		  name: 'func
		  data: (reverse result))))
	      ('pointer
	       (make-hir-type
		name: 'pointer
		data: (analyze-hir-type scope (hir-node-info node))))
	      (else
	       (let ((result (is-type-defined scope type)))
		 (unless result
		   (report-error *error-usage-of-undefined*
				 (format #f "~a is not defined as a type."
					 (hir-type-pretty-print type))
				 (hir-node-col node) (hir-node-row node)))
		 result)))))))

  (define (get-contained pointer)
    (if (or (eq? (hir-node-type pointer) 'pointer)
	    (eq? (hir-node-type pointer) 'deref))
	(get-contained (hir-node-info pointer))
	(hir-node-info pointer)))

  (define (analyze-pointer scope node)
    (let ((contained (get-contained node)))
      (case (hir-node-type contained)
	('symbol
	 (make-hir-type
	  name: 'pointer
	  data: (analyze scope contained)))
	('type
	 (make-hir-type
	  name: 'pointer
	  data: (analyze-hir-type scope contained)))
	(else
	 (report-error *error-improper-pointer*
		       ;; TODO: better error message?
		       (format #f "~a does not have a memory address to access"
			       ;; TODO: pretty print
			       contained)
		       (hir-node-col contained)
		       (hir-node-row contained))))))

  (define (analyze-deref scope node)
    (let ((contained (analyze scope (get-contained node))))
      (when contained
	;; (hir-type) here since (analyze) returns an (hir-type)
	(if (not (eq? (hir-type-name contained) 'pointer))
	    (report-error *error-deref-non-pointer*
			  (format #f "~a is not a pointer"
				  ;; TODO: pretty print
				  contained)
			  (hir-node-col contained)
			  (hir-node-row contained))
	    ;; return the dereferenced value
	    ;; no need to analyze again since it's already been analyzed by (analyze-pointer)
	    (hir-node-info contained)))))

  (define (analyze-symbol scope node) 
    (let ((variable (is-variable-defined scope (hir-node-info node))))
      (if (null? variable)
	  (report-error *error-usage-of-undefined*
			(format #f "~a is not defined" 
				(hir-node-info node))
			(hir-node-col node)
			(hir-node-row node))
	  (var-decl-type variable))))

  (define (analyze-func-call scope node)
    (let ((signature (analyze scope (function-call-name (hir-node-info node)))))
      (unless (null? signature)
	;; TODO: check for 'generic-func
	;; needs to check for proper types with the generic names
	;; the same generic type names must be the same types
	;; test the generic function body with the argument types
	;; if that passes then we can continue
	(if (not (eq? (hir-type-name signature) 'func))
	    (report-error *error-defined-is-not-function*
			  (format #f "~a is not a function"
				  ;; TODO: pretty print
				  (hir-node-info (function-call-name (hir-node-info node))))
			  (hir-node-col node) 
			  (hir-node-row node))
	    (let ((return-type (first (hir-type-data signature)))
		  (parameters (second (hir-type-data signature)))
		  (arguments (function-call-arguments (hir-node-info node))))
	      ;; check for arity
	      (if (not (= (length parameters) (length arguments)))
		  (report-error *error-incorrect-arity*
				(format #f "Expected ~a arguments got ~a"
					(length parameters)
					(length arguments))
				(hir-node-col node)
				(hir-node-row node))
		  ;; check each argument matches the parameter
		  (loop for i from 0 below (length arguments) do
			(let ((arg-type (analyze scope (nth arguments i))))
			  (when (not (hir-same-type arg-type (nth parameters i)))
			    (report-error *error-incorrect-argument-type*
					  (format #f "Expected ~a argument type got ~a"
						  (hir-type-pretty-print (nth parameters i))
						  (hir-type-pretty-print arg-type))
					  (hir-node-col (nth arguments i))
					  (hir-node-row (nth arguments i)))
			    (return #f)))))
	      return-type))))))

(module hir-opt *
  (import scheme chicken)
  (require-extension defstruct format loop)
  (declare (unit hir-opt) (uses utility errors hir static type-info))
  (import utility errors hir type-info
	  (prefix static static:))
  
  ;; The HIR provided are not guaranteed to be correct in regards to their types
  ;; and if given to the static analyzer would fail the analysis. Type analyzation takes
  ;; this HIR and generates a new HIR that removes some possibilities for failure. It does not
  ;; check if types are defined or perform any other analysis that the static analyzer would do.

  ;; Type analyzation is where generics are turned into their particular usage signatures,
  ;; where automatic dereferencing ocurs, and where macro expansion happens.

  (define (copy-node node)
    (make-hir-node type: (hir-node-type node)
		   info: (hir-node-info node)
		   col: (hir-node-col node)
		   row: (hir-node-row node)))
  
  (define (analyze defs node)
    (when node
      (case (hir-node-type node)
	('define-syntax
	  ;; We don't want macro definitions in the HIR
	  ;; since we already processed them
	  #f)
	('func-call
	 (analyze-func-call defs node))
	('set
	 (analyze-set defs node))
	;; if no special handling is required just return the node
	(else
	 node))))

  ;; TODO: check if this is defining a generic function
  ;; and if so look for every usage of the generic and
  ;; generate the proper function signatures
  ;; TODO: check if a list literal is being used and generate
  ;; a (define) with a tmp name for the list literal (same for
  ;; function calls, set!, if, operator)
  (define (analyze-define-val defs node)
    (let ((value (second (hir-node-info node))))
      node))

  ;; TODO: same as analyze-define-val
  (define (analyze-define-type-val defs node)
    '())

  ;; TODO: set! analyzes whether a pointer value
  ;; is being assigned to and if so 
  (define (analyze-set defs node)
    '()))

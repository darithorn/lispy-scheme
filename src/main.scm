(module main ()
  (import scheme chicken extras)
  (require-extension defstruct loop format)
  (declare (uses errors parser static hir))
  (import utility errors parser hir type-info
	  (prefix static static:))

  (with-lexer (main-lexer "src/test.scm")
	      ;; this will create a containing list for every expression in the file
	      ;; now parsing multiple expressions is as easy as one call to parse-list
	      (let ((eval-expr (parse-list main-lexer #t)))
		(let ((global-scope (static:new-hir-scope '())))
		  (static:add-type global-scope (make-hir-type name: 'i32 data: '()))
		  (static:add-type global-scope (make-hir-type name: 'f32 data: '()))
		  (static:add-variable global-scope (make-var-decl name: "bar"
								   type: (make-hir-type name: 'i32 data: '())))
		  ;; (setf *built-ins* (parse-built-in-patterns "builtins.lisp"))
		  (loop for expr in (form-expr eval-expr) do
			;;	   (format t "expr: ~a~%" expr)
			(format #t "~a~%" (static:analyze global-scope (reinterpret expr))))
		  (when (> (length *errors*) 0)
		    (loop for err in *errors* do
			  (format #t "~a" (error->string err))))))))
